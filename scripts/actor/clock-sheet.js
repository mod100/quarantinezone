const { api, sheets } = foundry.applications;

/**
 * Extend the basic ActorSheet with some modifications for clocks
 * @extends {ActorSheetV2}
 */
export class qzClockSheet extends api.HandlebarsApplicationMixin(
  sheets.ActorSheetV2
) {
  static DEFAULT_OPTIONS = {
    classes: ["quarantinezone", "sheet", "actor"],
    position: {
      width: 265,
      height: 375,
    },
    window: {
      title: "Clock",
    },
    tag: "form",
    form: {
      submitOnChange: true,
    },
  };

  static PARTS = {
    form: {
      template: "systems/quarantinezone/templates/actor/clock-sheet.hbs",
    },
  };

  /** @override */
  async _prepareContext(options) {
    const context = {
      owner: this.document.isOwner,
      editable: this.isEditable,
      actor: this.actor,
      system: this.actor.system,
      selectValues: { 4: 4, 6: 6, 8: 8 },
    };

    return context;
  }

  async _processSubmitData(event, form, submitData) {
    const overrides = foundry.utils.flattenObject(this.actor.overrides);
    for (let k of Object.keys(overrides)) delete submitData[k];

    let image_path = `/systems/quarantinezone/images/Progress Clock ${submitData.system.type}-${submitData.system.value}.svg`;
    submitData.img = image_path;
    submitData.token = { img: image_path };
    let data = [];
    let update = {
      texture: { src: image_path },
      width: 1,
      height: 1,
      scale: 1,
      mirrorX: false,
      mirrorY: false,
      tint: "",
      displayName: 50,
    };

    let tokens = this.actor.getActiveTokens();
    tokens.forEach(function (token) {
      data.push(foundry.utils.mergeObject({ _id: token.id }, update));
    });
    await TokenDocument.updateDocuments(data, { parent: game.scenes.current });

    await this.document.update(submitData);
  }

  /** @override */
  // async _onChangeForm(formData, event) {
  //

  //   // Update the Actor
  //   return this.object.update(formData);
  // }
}
