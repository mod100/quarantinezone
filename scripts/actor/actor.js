/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */

export function attributeSort(a, b) {
  if (a.order < b.order) return -1;
  if (a.order > b.order) return 1;
  return 0;
}

export class qzActor extends Actor {
  /**
   * Augment the basic actor data with additional dynamic data.
   */
  prepareData() {
    super.prepareData();

    const actorData = this;

    const data = actorData;
    const flags = actorData.flags;

    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    if (actorData.type === "character") this._prepareCharacterData(actorData);
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    const data = actorData;

    // Make modifications to data here. For example:

    // Loop through ability scores, and add their modifiers to our sheet output.
    // for (let [key, ability] of Object.entries(data.abilities)) {
    //   // Calculate the modifier using d20 rules.
    //   ability.mod = Math.floor((ability.value - 10) / 2);
    // }

    // Set the health to add might to the max number
    const currMaxHealth = data.system.health.max;
    const healthMod =
      data.system.explorationAttributes[data.system.health.attribute].value;

    const maxHealth = currMaxHealth + healthMod;

    data.system.health.max = maxHealth;

    // Add correct values to encounter attributes

    for (let [key, ability] of Object.entries(
      data.system.encounterAttributes
    )) {
      const abilityModifier =
        data.system.explorationAttributes[ability.attribute].value;
      const maxValue = ability.maxModifier + abilityModifier;

      data.system.encounterAttributes[key].value = maxValue;
    }
  }
}
