import { doQZRoll } from "../roll.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class qzActorSheet extends ActorSheet {
  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["quarantinezone", "sheet", "actor"],
      template: "systems/quarantinezone/templates/actor/actor-sheet.hbs",
      width: 915,
      height: 650,
      tabs: [
        {
          navSelector: ".sheet-tabs",
          contentSelector: ".sheet-body",
          initial: "description",
        },
      ],
    });
  }

  /* -------------------------------------------- */

  /** @override */
  async getData(options) {
    const baseData = super.getData();

    let tempItems = this.actor.toObject(false).items;
    tempItems.sort((a, b) => (a.sort || 0) - (b.sort || 0));

    let archetypesForSheet = { "": "--- Select ---" };

    for (const a of CONFIG.QZ.archetypes) {
      archetypesForSheet[a] = a;
    }

    let sheetData = {
      owner: baseData.actor.isOwner,
      editable: this.isEditable,
      actor: baseData.actor,
      system: baseData.actor.system,
      rollData: baseData.actor.getRollData.bind(this.actor),
      items: tempItems,
      dtypes: ["String", "Number", "Boolean"],
      archetypes: archetypesForSheet,
    };

    // Prepare items.
    if (this.actor.type == "character") {
      this._prepareCharacterItems(sheetData);
    }

    const effects = this.prepareActiveEffectCategories(
      // A generator that returns all effects stored on the actor
      // as well as any items
      this.actor.allApplicableEffects()
    );

    sheetData.effects = effects;

    return sheetData;
  }

  prepareActiveEffectCategories(effects) {
    // Define effect header categories
    const categories = {
      temporary: {
        type: "temporary",
        label: game.i18n.localize("QZ.Effect.Temporary"),
        effects: [],
      },
      passive: {
        type: "passive",
        label: game.i18n.localize("QZ.Effect.Passive"),
        effects: [],
      },
      inactive: {
        type: "inactive",
        label: game.i18n.localize("QZ.Effect.Inactive"),
        effects: [],
      },
    };

    // Iterate over active effects, classifying them into categories
    for (let e of effects) {
      if (e.disabled) categories.inactive.effects.push(e);
      else if (e.isTemporary) categories.temporary.effects.push(e);
      else categories.passive.effects.push(e);
    }
    return categories;
  }

  onManageActiveEffect(event, owner) {
    event.preventDefault();
    const a = event.currentTarget;
    const li = a.closest("li");
    const effect = li.dataset.effectId
      ? owner.effects.get(li.dataset.effectId)
      : null;
    switch (a.dataset.action) {
      case "create":
        return owner.createEmbeddedDocuments("ActiveEffect", [
          {
            name: game.i18n.format("DOCUMENT.New", {
              type: game.i18n.localize("DOCUMENT.ActiveEffect"),
            }),
            icon: "icons/svg/aura.svg",
            origin: owner.uuid,
            "duration.rounds":
              li.dataset.effectType === "temporary" ? 1 : undefined,
            disabled: li.dataset.effectType === "inactive",
          },
        ]);
      case "edit":
        return effect.sheet.render(true);
      case "delete":
        return effect.delete();
      case "toggle":
        return effect.update({ disabled: !effect.disabled });
    }
  }

  get template() {
    const path = "systems/quarantinezone/templates/actor";
    // Return a single sheet for all item types.
    //return `${path}/item-sheet.html`;
    // Alternatively, you could use the following return statement to do a
    // unique item sheet by type, like `weapon-sheet.html`.

    return `${path}/${this.actor.type}-sheet.hbs`;
  }

  /**
   * Prepare the data for the character, split the owned items into the items
   * that are gear and those that are moves.
   */
  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;
    const gear = [];
    const encounterActions = [];
    const explorationActions = [];
    const traits = [];
    const permanentInjuries = [];
    const reputations = [];
    const weapons = [];
    const personalAgendas = [];
    const archetypeAgendas = [];
    const origins = [];

    for (let i of sheetData.items) {
      i.img = i.img || DEFAULT_TOKEN;

      if (i.type === "gear") {
        gear.push(i);
      } else if (i.type === "encounterAction") {
        encounterActions.push(i);
      } else if (i.type === "explorationAction") {
        explorationActions.push(i);
      } else if (i.type === "trait") {
        traits.push(i);
      } else if (i.type === "permanentInjury") {
        permanentInjuries.push(i);
      } else if (i.type === "reputation") {
        reputations.push(i);
      } else if (i.type === "weapon") {
        weapons.push(i);
      } else if (i.type === "agenda") {
        if (i.system.type === "Archetype") {
          archetypeAgendas.push(i);
        } else {
          personalAgendas.push(i);
        }
      } else if (i.type === "origin") {
        origins.push(i);
      }

      actorData.gear = gear;
      actorData.encounterActions = encounterActions;
      actorData.explorationActions = explorationActions;
      actorData.traits = traits;
      actorData.permanentInjuries = permanentInjuries;
      actorData.reputations = reputations;
      actorData.weapons = weapons;
      actorData.personalAgendas = personalAgendas;
      actorData.archetypeAgendas = archetypeAgendas;
      actorData.origins = origins;
    }
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Add Inventory Item
    html.find(".item-create").click(this._onItemCreate.bind(this));

    // Active Effect management
    html.on("click", ".effect-control", (ev) => {
      const row = ev.currentTarget.closest("li");
      const document =
        row.dataset.parentId === this.actor.id
          ? this.actor
          : this.actor.items.get(row.dataset.parentId);
      this.onManageActiveEffect(ev, document);
    });

    // Update Inventory Item
    html.find(".item-edit").click((ev) => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find(".item-delete").click((ev) => {
      const li = $(ev.currentTarget).parents(".item");
      Item.deleteDocuments([li.data("itemId")], { parent: this.actor });
      li.slideUp(200, () => this.render(false));
    });

    // Rollable abilities.
    html.find(".rollable-encounter").click(this._onRollEncounter.bind(this));
    html
      .find(".rollable-exploration")
      .click(this._onRollExploration.bind(this));

    html.find(".rollable-move-image").click(this._onMoveRoll.bind(this));

    html.find(".chatable-owned-item").click(this._onItemClick.bind(this));

    // Drag events for macros.
    if (this.actor.isOwner) {
      let handler = (ev) => this._onDragStart(ev);
      // Find all items on the character sheet.
      html.find("li.item").each((i, li) => {
        // Ignore for the header row.
        if (li.classList.contains("item-header")) return;
        // Add draggable attribute and dragstart listener.
        li.setAttribute("draggable", true);
        li.addEventListener("dragstart", handler, false);
      });
    }
  }

  /* -------------------------------------------- */

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data,
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // Finally, create the item!
    return Item.createDocuments([itemData], { parent: this.actor });
  }

  _onItemClick(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    this.actor.items.forEach((item) => {
      if (item.type !== "move" && item.id === dataset.item) {
        doQZRoll(this.actor, item.type, item.name);
      }
    });
  }

  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  _onRollExploration(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    doQZRoll(this.actor, "explorationAttribute", dataset.label);
  }

  _onRollEncounter(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    doQZRoll(this.actor, "encounterAttribute", dataset.label);
  }

  _onMoveRoll(event) {
    event.preventDefault();

    const element = event.currentTarget;
    const dataset = element.dataset;

    if (dataset.type === "explorationAction") {
      doQZRoll(this.actor, "explorationAction", dataset.move);
    } else if (dataset.type === "encounterAction") {
      doQZRoll(this.actor, "encounterAction", dataset.move);
    }
  }

  static async _createDoc(event, target) {
    // Retrieve the configured document class for Item or ActiveEffect
    const docCls = getDocumentClass(target.dataset.documentClass);
    // Prepare the document creation data by initializing it a default name.
    const docData = {
      name: docCls.defaultName({
        // defaultName handles an undefined type gracefully
        type: target.dataset.type,
        parent: this.actor,
      }),
    };
    // Loop through the dataset and add it to our docData
    for (const [dataKey, value] of Object.entries(target.dataset)) {
      // These data attributes are reserved for the action handling
      if (["action", "documentClass"].includes(dataKey)) continue;
      // Nested properties require dot notation in the HTML, e.g. anything with `system`
      // An example exists in spells.hbs, with `data-system.spell-level`
      // which turns into the dataKey 'system.spellLevel'
      foundry.utils.setProperty(docData, dataKey, value);
    }

    // Finally, create the embedded document!
    await docCls.create(docData, { parent: this.actor });
  }
}
