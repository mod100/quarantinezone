import { QZRollDialog } from "./roll/roll-dialog.js";

export function innerQZRoll(
  rollingObject,
  type,
  objectName,
  bonus,
  weaponBonus,
  numDice
) {
  let wBonus = 0;

  if (weaponBonus) {
    wBonus = weaponBonus;
  }

  if (type === "encounterAttribute") {
    const data = rollingObject.system;

    for (let [key, attr] of Object.entries(data.encounterAttributes)) {
      if (key === objectName) {
        let roll = new Roll(`${numDice}d6+${attr.value}+${bonus}`, data);
        let label = `Rolling <b>${key}</b>`;
        roll.toMessage({
          speaker: ChatMessage.getSpeaker({ actor: rollingObject }),
          flavor: label,
        });
      }
    }
  } else if (type === "explorationAttribute") {
    const data = rollingObject.system;

    for (let [key, attr] of Object.entries(data.explorationAttributes)) {
      if (attr.label === objectName) {
        let roll = new Roll(`${numDice}d6+${attr.value}+${bonus}`, data);
        let label = `Rolling <b>${attr.label}</b>`;
        roll.toMessage({
          speaker: ChatMessage.getSpeaker({ actor: rollingObject }),
          flavor: label,
        });
      }
    }
  } else if (type === "explorationAction") {
    let action = undefined;
    const data = rollingObject.system;
    const items = rollingObject.items;

    items.forEach((item) => {
      if (item.type === "explorationAction" && item.name === objectName) {
        action = item;
      }
    });

    // Find the attribute the move is related to so we can use the modifier
    // for the roll correctly
    for (let [key, attr] of Object.entries(data.explorationAttributes)) {
      if (action && attr.label === action.system.type) {
        new Roll(`${numDice}d6+${attr.value}+${bonus}+${wBonus}`, data)
          .evaluate()
          .then((roll) => {
            let resultText = "Failure";

            for (const boundary in action.system.boundaries) {
              if (roll.total >= action.system.boundaries[boundary].minRoll) {
                resultText = action.system.boundaries[boundary].description;
              }
            }

            let className = "";
            let extraElement = "";

            if (resultText === "Failure") {
              className = "result-text-failure";
            } else if (resultText === "Partial Success") {
              className = "result-text-partial";
            } else if (resultText === "Success") {
              className = "result-text-success";
            } else if (resultText === "Advanced Success") {
              className = "result-text-advanced-success";
              extraElement = " <i class='fa-solid fa-star'></i> ";
            }

            const chatText = `<div class="flexcol"><header class="chat-header flexrow"><img class="chatimage" src='${action.img}'/><h3>${action.name}</h3></div><p>${action.system.description}</p><div class="${className}">${extraElement}${resultText}${extraElement}</div></div>`;

            roll.toMessage({
              speaker: ChatMessage.getSpeaker({ actor: rollingObject }),
              flavor: chatText,
            });
          });
      }
    }
  } else if (type === "encounterAction") {
    let action = undefined;
    const data = rollingObject.system;
    const items = rollingObject.items;

    items.forEach((item) => {
      if (item.type === "encounterAction" && item.name === objectName) {
        action = item;
      }
    });
    const chatText = `<div class="flexcol"><header class="chat-header flexrow"><img class="chatimage" src='${action.img}'/><h3>${action.name}</h3></div><p>${action.system.description}</p></div>`;
    ChatMessage.create({
      speaker: ChatMessage.getSpeaker({ actor: rollingObject }),
      content: chatText,
    });
  } else if (type === "weapon") {
    const data = rollingObject.system;
    const items = rollingObject.items;

    items.forEach((item) => {
      if (item.type === type && item.name === objectName) {
        let messageDetail = `<div class="flexcol">
                         <header class="chat-header flexrow"><img class="chatimage" src='${item.img}'/><h3>${item.name}</h3>
                         </div>
                         <p><strong>Range:</strong> ${item.system.range.type}</p>`;

        if (item.system.range.type == "Ranged") {
          messageDetail =
            messageDetail +
            `<table class="chat-table"><thead><tr><th>S</th><th>M</th><th>L</th><th>X</th></tr></thead>
            <tbody>
              <tr>
                <td>${item.system.range.short.modifier}</td>
                <td>${item.system.range.medium.modifier}</td>
                <td>${item.system.range.long.modifier}</td>
                <td>${item.system.range.extreme.modifier}</td>
              </tr>
            </tbody></table>`;
        }

        messageDetail =
          messageDetail +
          `<p><strong>Damage:</strong> ${item.system.damage.min}/${item.system.damage.max} ${item.system.damage.type}</p>
                         <p><strong>Traits:</strong> ${item.system.traits}</p>
                         <p><strong>Actions:</strong> ${item.system.description}</p></div>`;

        new Roll(
          `${numDice}d6+${data.encounterAttributes.attack.value}+${bonus}+${wBonus}`,
          data
        )
          .evaluate()
          .then((roll) => {
            let critSuccess = true;

            for (const d of roll.dice[0].results) {
              if (d.result !== 6) critSuccess = false;
            }

            if (critSuccess) {
              messageDetail =
                messageDetail +
                `<div class="result-text-advanced-success">
                    Critical ++
                  </div>`;
            }

            roll.toMessage({
              speaker: ChatMessage.getSpeaker({ actor: rollingObject }),
              flavor: messageDetail,
            });
          });
      }
    });
  } else if (
    type === "gear" ||
    type === "reputation" ||
    type === "permanentInjury" ||
    type === "trait" ||
    type === "origin" ||
    type === "agenda"
  ) {
    // Roll for an item
    const items = rollingObject.items;

    items.forEach((item) => {
      if (item.type === type && item.name === objectName) {
        let messageDetail = "";

        if (item.type === "reputation") {
          messageDetail = `<div class="flexcol"><header class="chat-header flexrow"><img class="chatimage" src='${item.img}'/><h3>${item.name}</h3></div><p>${item.system.description}</p><p>Current reputation: ${item.system.level.value}/${item.system.level.max}</p></div>`;
        } else {
          messageDetail = `<div class="flexcol"><header class="chat-header flexrow"><img class="chatimage" src='${item.img}'/><h3>${item.name}</h3></div><p>${item.system.description}</p></div>`;
        }

        const chatData = {
          speaker: ChatMessage.getSpeaker({ actor: rollingObject }),
          content: messageDetail,
        };

        ChatMessage.create(chatData);
      }
    });
  }
}

export async function doQZRoll(rollingObject, type, objectName) {
  if (
    type === "weapon" ||
    type === "explorationAttribute" ||
    type === "encounterAttribute" ||
    type === "explorationAction"
  ) {
    new QZRollDialog(rollingObject, type, objectName).render(true);
  } else {
    innerQZRoll(rollingObject, type, objectName);
  }
}

Handlebars.registerHelper("numberToText", function (value) {
  if (value === 1) {
    return "one";
  } else if (value === 2) {
    return "two";
  } else if (value === 3) {
    return "three";
  } else if (value === 4) {
    return "four";
  } else if (value === 5) {
    return "five";
  } else if (value === 6) {
    return "six";
  } else {
    return "";
  }
});

Handlebars.registerHelper("lessThanOrEqual", function (value1, value2) {
  if (value1 <= value2) return true;
  else return false;
});
