/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class qzItemSheet extends ItemSheet {
  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["quarantinezone", "sheet", "item"],
      width: 520,
      height: 480,
      tabs: [
        {
          navSelector: ".sheet-tabs",
          contentSelector: ".sheet-body",
          initial: "description",
        },
      ],
    });
  }

  /** @override */
  get template() {
    const path = "systems/quarantinezone/templates/item";
    // Return a single sheet for all item types.
    //return `${path}/item-sheet.html`;
    // Alternatively, you could use the following return statement to do a
    // unique item sheet by type, like `weapon-sheet.html`.

    return `${path}/${this.item.type}-sheet.hbs`;
  }

  /* -------------------------------------------- */

  /** @override */
  async getData(options) {
    const baseData = super.getData();

    let attributesForSheet = { "": "--- Select ---" };

    for (const a of CONFIG.QZ.attributes) {
      attributesForSheet[a] = a;
    }

    let sheetData = {
      owner: this.item.isOwner,
      editable: this.isEditable,
      item: baseData.item,
      data: baseData.item.system.data,
      system: baseData.item.system,
      rangeOptions: { Close: "Close", Ranged: "Ranged" },
      agendaType: {
        "": "--- Select ---",
        Personal: "Personal",
        Archetype: "Archetype",
      },
      attributes: attributesForSheet,
    };

    return sheetData;
  }

  /* -------------------------------------------- */

  /** @override */
  setPosition(options = {}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - 192;
    sheetBody.css("height", bodyHeight);
    return position;
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Add or Remove Attribute
    html
      .find(".boundaries")
      .on(
        "click",
        ".boundary-control",
        this._onClickBoundaryControl.bind(this)
      );
  }

  async _onClickBoundaryControl(event) {
    event.preventDefault();
    const a = event.currentTarget;
    const action = a.dataset.action;
    const boundaries = this.object.system.boundaries;
    const form = this.form;

    // Add new boundary
    if (action === "create") {
      const nk = Object.keys(boundaries).length + 1;
      let newKey = document.createElement("div");

      newKey.innerHTML = `<input type="text" name="system.boundaries.${nk}.key" value="boundary${nk}"/>`;
      newKey = newKey.children[0];
      form.appendChild(newKey);
      await this._onSubmit(event);
    }

    // Remove existing boundary
    else if (action === "delete") {
      const li = a.closest(".boundary");
      li.parentElement.removeChild(li);
      await this._onSubmit(event);
    }
  }

  _updateObject(event, formData) {
    // Handle boundary deletions

    const regex = new RegExp(/system\.boundaries\.[0-9]*\.key/, "g");

    let keys = [];

    for (let [key, value] of Object.entries(formData)) {
      if (key.match(regex)) {
        keys.push(value);
      }
    }

    if (this.object.system.boundaries) {
      for (let [key, boundary] of Object.entries(
        this.object.system.boundaries
      )) {
        if (!keys.includes(boundary.key)) {
          delete this.object.system.boundaries[key];
        }
      }
    }

    // Update the Item
    return this.object.update(formData);
  }
}
