// Import Modules
import { qzActor } from "./actor/actor.js";
import { qzActorSheet } from "./actor/actor-sheet.js";
import { qzItem } from "./item/item.js";
import { qzItemSheet } from "./item/item-sheet.js";
import { qzClockSheet } from "./actor/clock-sheet.js";
import QZ from "./config.js";

Hooks.once("init", async function () {
  game.quarantinezone = {
    qzActor,
    qzItem,
    rollItemMacro,
  };
  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.Combat.initiative = {
    formula: "@encounterAttributes.agility.value",
  };

  // Define custom Entity classes
  CONFIG.Actor.documentClass = qzActor;
  CONFIG.Item.documentClass = qzItem;

  CONFIG.QZ = QZ;

  // Define custom Entity classes. This will override the default Actor and
  // Item classes to instead use our extended versions.
  Actors.unregisterSheet("core", ActorSheet);
  //Actors.registerSheet("quarantinezone", qzActorSheet, { makeDefault: true });
  Actors.registerSheet("quarantinezone", qzActorSheet, {
    types: ["character"],
    makeDefault: true,
  });
  Actors.registerSheet("quarantinezone", qzActorSheet, {
    types: ["npc"],
    makeDefault: true,
  });
  Actors.registerSheet("quarantinezone", qzClockSheet, {
    types: ["clock"],
    makeDefault: true,
  });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("quarantinezone", qzItemSheet, { makeDefault: true });

  Actors.registeredSheets.forEach((element) => console.log(element.Actor.name));

  // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper("concat", function () {
    var outStr = "";
    for (var arg in arguments) {
      if (typeof arguments[arg] != "object") {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper("toLowerCase", function (str) {
    return str.toLowerCase();
  });

  /**
   * Create appropriate progress clock
   */

  Handlebars.registerHelper(
    "progress-clock",
    function (parameter_name, type, current_value, uniq_id) {
      let html = "";

      if (current_value === null || current_value === "null") {
        current_value = 0;
      }

      if (parseInt(current_value) > parseInt(type)) {
        current_value = type;
      }

      // Label for 0
      html += `<label class="clock-zero-label" for="clock-0-${uniq_id}}"><i class="fab fa-creative-commons-zero nullifier"></i></label>`;
      html += `<div id="progress-clock-${uniq_id}" class="progress-clock clock-${type} clock-${type}-${current_value}" style="background-image:url('/systems/quarantinezone/images/Progress Clock ${type}-${current_value}.svg');">`;

      let zero_checked =
        parseInt(current_value) === 0 ? 'checked="checked"' : "";
      html += `<input type="radio" value="0" id="clock-0-${uniq_id}}" name="${parameter_name}" ${zero_checked}>`;

      for (let i = 1; i <= parseInt(type); i++) {
        let checked = parseInt(current_value) === i ? 'checked="checked"' : "";
        html += `
        <input type="radio" value="${i}" id="clock-${i}-${uniq_id}" name="${parameter_name}" ${checked}>
        <label for="clock-${i}-${uniq_id}"></label>
      `;
      }

      html += `</div>`;
      return html;
    }
  );

  loadTemplates([
    // Actor partials.
    "systems/quarantinezone/templates/actor/parts/actor-effects.hbs",
    "systems/quarantinezone/templates/actor/parts/actor-encounter-actions.hbs",
    "systems/quarantinezone/templates/actor/parts/actor-exploration-actions.hbs",
    "systems/quarantinezone/templates/actor/parts/actor-gear.hbs",
    "systems/quarantinezone/templates/actor/parts/actor-perm-injuries.hbs",
    "systems/quarantinezone/templates/actor/parts/actor-reputation.hbs",
    "systems/quarantinezone/templates/actor/parts/actor-resources.hbs",
    "systems/quarantinezone/templates/actor/parts/actor-traits.hbs",
    "systems/quarantinezone/templates/actor/parts/actor-agendas.hbs",
    "systems/quarantinezone/templates/actor/parts/actor-origins.hbs",
  ]);
});

Hooks.once("ready", async function () {
  Hooks.on("hotbarDrop", (bar, data, slot) => {
    if (data.type === "Item") {
      handleDroppedItem(bar, data, slot);
      return false;
    }
  });
});

/* -------------------------------------------- */
/*  Hotbar Macros                               */
/* -------------------------------------------- */

async function handleDroppedItem(bar, data, slot) {
  const itemData = await Item.implementation.fromDropData(data);
  const macroData = { type: "script", scope: "actor" };

  if (!itemData)
    return ui.notifications.warn(game.i18n.localize("QZ.UnownedWarn"));
  foundry.utils.mergeObject(macroData, {
    name: itemData.name,
    img: itemData.img,
    command: `game.quarantinezone.rollItemMacro("${itemData.name}")`,
    flags: { "quarantinezone.itemMacro": true },
  });

  const macro =
    game.macros.find(
      (m) =>
        m.name === macroData.name &&
        m.command === macroData.command &&
        m.author.isSelf
    ) || (await Macro.create(macroData));
  game.user.assignHotbarMacro(macro, slot);
}

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {string} itemName
 * @return {Promise}
 */
function rollItemMacro(itemName) {
  const speaker = ChatMessage.getSpeaker();
  let actor;
  if (speaker.token) actor = game.actors.tokens[speaker.token];
  if (!actor) actor = game.actors.get(speaker.actor);
  const item = actor ? actor.items.find((i) => i.name === itemName) : null;
  if (!item)
    return ui.notifications.warn(
      `Your controlled Actor does not have an item named ${itemName}`
    );

  // Trigger the item roll
  return item.roll();
}
