const QZ = {};

QZ.archetypes = ["Slick", "Survivor", "Specialist", "Muscle", "Freak"];
QZ.rangeTypes = ["Close", "Ranged"];
QZ.attributes = ["Might", "Instinct", "Finesse", "Smarts", "Compel", "Weird"];

export default QZ;
