import { innerQZRoll } from "../roll.js";

const { ApplicationV2, HandlebarsApplicationMixin } = foundry.applications.api;

export class QZRollDialog extends HandlebarsApplicationMixin(ApplicationV2) {
  constructor(rollingObject, type, objectName) {
    super({});

    this.rollingObject = rollingObject;
    this.type = type;
    this.objectName = objectName;
    this.numDice = 2;
    this.selectedRange = "";
    this.bonus = 0;
  }

  static DEFAULT_OPTIONS = {
    classes: ["quarantinezone"],
    position: {
      width: 600,
      height: 400,
    },
    window: {
      title: "Roll Attack",
    },
    tag: "form",
    form: {
      submitOnChange: false,
      closeOnSubmit: true,
      handler: QZRollDialog.formHandler,
    },
    actions: {
      incrementDice: this._incrementDice,
      decrementDice: this._decrementDice,
    },
  };

  static PARTS = {
    form: {
      template: "systems/quarantinezone/templates/roll/qz-roll.hbs",
    },
  };

  static _incrementDice(event, target) {
    if (this.numDice < 6) this.numDice = this.numDice + 1;
    this.storeFormValues();
    this.render(true);
  }

  static _decrementDice(event, target) {
    if (this.numDice > 0) this.numDice = this.numDice - 1;
    this.storeFormValues();
    this.render(true);
  }

  storeFormValues() {
    this.bonus = this.element.querySelector("#bonus")
      ? this.element.querySelector("#bonus").value
      : "";
    this.selectedRange = this.element.querySelector("#range-select")
      ? this.element.querySelector("#range-select").value
      : "";
  }

  async _prepareContext(options) {
    const possibleRanges = {};
    let range = "";

    if (this.type === "weapon") {
      //if it's a weapon then ask for the range
      const items = this.rollingObject.items;

      items.forEach((item) => {
        if (item.type === this.type && item.name === this.objectName) {
          range = item.system.range.type;

          if (item.system.range.type === "Ranged") {
            if (
              item.system.range.short.modifier &&
              item.system.range.short.modifier != "X"
            ) {
              possibleRanges[
                item.system.range.short.modifier
              ] = `Short: ${item.system.range.short.modifier}`;
            }

            if (
              item.system.range.medium.modifier &&
              item.system.range.medium.modifier != "X"
            ) {
              possibleRanges[
                item.system.range.medium.modifier
              ] = `Medium: ${item.system.range.medium.modifier}`;
            }

            if (
              item.system.range.long.modifier &&
              item.system.range.long.modifier != "X"
            ) {
              possibleRanges[
                item.system.range.long.modifier
              ] = `Long: ${item.system.range.long.modifier}`;
            }

            if (
              item.system.range.extreme.modifier &&
              item.system.range.extreme.modifier != "X"
            ) {
              possibleRanges[
                item.system.range.extreme.modifier
              ] = `Extreme: ${item.system.range.extreme.modifier}`;
            }
          }
        }
      });
    }

    const context = {
      type: this.type,
      possibleRanges,
      range,
      numDice: this.numDice,
      possibleDice: [1, 2, 3, 4, 5, 6],
      selectedRange: this.selectedRange,
      bonus: this.bonus,
      buttons: [
        { type: "submit", icon: "fa-solid fa-save", label: "SETTINGS.Save" },
        {
          type: "button",
          action: "incrementDice",
          //   icon: "fa-solid fa-undo",
          label: "Add Dice",
        },
        {
          type: "button",
          action: "decrementDice",
          //   icon: "fa-solid fa-undo",
          label: "Remove Dice",
        },
      ],
    };

    return context;
  }

  static async formHandler(event, form, formData) {
    innerQZRoll(
      this.rollingObject,
      this.type,
      this.objectName,
      formData["object"]["bonus"],
      formData["object"]["range-select"],
      formData["object"]["num-dice"]
    );
  }
}
